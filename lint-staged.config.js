module.exports = {
  '*.js': ['npm run lint:eslint', 'npm run lint:prettier', 'git add'],
  '*.graphql': ['npm run lint:prettier', 'git add']
}
