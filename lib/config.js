const convict = require('convict')

const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV'
  },
  port: {
    doc: 'Server port number',
    format: 'int',
    default: 8000,
    env: 'SERVER_PORT'
  }
})

config.validate({ allowed: 'strict' })

module.exports = config
