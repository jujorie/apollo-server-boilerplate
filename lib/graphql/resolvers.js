const { mergeResolvers } = require('merge-graphql-schemas')
const { sync: glob } = require('glob')
const { join } = require('path')

const files = glob(join(__dirname, 'resolvers', '**', '*.resolver.js'))

const resolvers = mergeResolvers(files.map(f => require(f)))

module.exports = { resolvers }
