const { join } = require('path')
const { fileLoader, mergeTypes } = require('merge-graphql-schemas')

const typesArray = fileLoader(join(__dirname, './types'))
const typeDefs = mergeTypes(typesArray, { all: true })

module.exports = { typeDefs }
