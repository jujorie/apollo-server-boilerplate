const Express = require('express')

const { applyCommonMiddleware, apolloMiddleware } = require('./middleware')

const app = Express()

apolloMiddleware(app)
applyCommonMiddleware(app)

module.exports = { app }
