const { BAD_REQUEST, INTERNAL_SERVER_ERROR } = require('http-status-codes')

const errorHandlerMiddleware = (err, req, res, next) => {
  if (res.statusCode < BAD_REQUEST) {
    res.statusCode = INTERNAL_SERVER_ERROR
  }
  res.json({
    status: res.statusCode,
    message: err.message
  })
}

module.exports = { errorHandlerMiddleware }
