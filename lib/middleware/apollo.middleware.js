const { ApolloServer } = require('apollo-server-express')

const { typeDefs, resolvers } = require('../graphql')

const apolloMiddleware = app => {
  const server = new ApolloServer({
    typeDefs,
    resolvers
  })
  server.applyMiddleware({ app })
}

module.exports = { apolloMiddleware }
