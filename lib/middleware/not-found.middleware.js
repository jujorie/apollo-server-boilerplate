const { NOT_FOUND } = require('http-status-codes')

const notFoundMiddleware = (req, res, next) => {
  res.statusCode = NOT_FOUND
  next(new Error('NOT FOUND'))
}

module.exports = { notFoundMiddleware }
