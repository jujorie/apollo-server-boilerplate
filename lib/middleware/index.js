const { errorHandlerMiddleware } = require('./error-handler.middleware')
const { notFoundMiddleware } = require('./not-found.middleware')
const { apolloMiddleware } = require('./apollo.middleware')
const { applyCommonMiddleware } = require('./apply-common-middleware')

module.exports = {
  errorHandlerMiddleware,
  notFoundMiddleware,
  apolloMiddleware,
  applyCommonMiddleware
}
