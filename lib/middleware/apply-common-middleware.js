const { errorHandlerMiddleware } = require('./error-handler.middleware')
const { notFoundMiddleware } = require('./not-found.middleware')

/**
 * Apply common middlewares
 * @param {Express.Application} app - Express application
 */
const applyCommonMiddleware = app => {
  app.use(notFoundMiddleware)
  app.use(errorHandlerMiddleware)
}

module.exports = { applyCommonMiddleware }
