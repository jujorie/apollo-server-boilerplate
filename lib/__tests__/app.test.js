const request = require('supertest')
const { app } = require('../app')

describe('app', () => {
  it('Must get graphql ep', async () => {
    const response = await request(app)
      .get('/graphql')
      .send()
    expect(response.status).toBe(400)
  })
})
