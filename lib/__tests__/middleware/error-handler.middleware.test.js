/**
 * @author Telefonica
 * @description Auth Sever
 */

'use strict'

const request = require('supertest')
const express = require('express')

const {
  errorHandlerMiddleware
} = require('../../middleware/error-handler.middleware')
const { UNAUTHORIZED, INTERNAL_SERVER_ERROR } = require('http-status-codes')

describe('middlewares/errorHandlerMiddleware', () => {
  let app

  beforeEach(() => {
    app = express()
  })

  it('Must get internal server error', async () => {
    app.use('/', (req, res, next) => {
      next(new Error('INTERNAL_SERVER_ERROR error'))
    })
    app.use(errorHandlerMiddleware)

    const res = await request(app)
      .get('/')
      .send()

    expect(res.status).toBe(INTERNAL_SERVER_ERROR)
    expect(res.body.status).toBe(INTERNAL_SERVER_ERROR)
  })

  it('Must get a forbidden error', async () => {
    app.use('/', (req, res, next) => {
      res.status(UNAUTHORIZED)
      next(new Error('UNAUTHORIZED error'))
    })
    app.use(errorHandlerMiddleware)

    const res = await request(app)
      .get('/')
      .send()

    expect(res.status).toBe(UNAUTHORIZED)
    expect(res.body.status).toBe(UNAUTHORIZED)
  })
})
