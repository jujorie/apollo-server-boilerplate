/**
 * @author Telefonica
 * @description Auth Sever
 */

'use strict'

const request = require('supertest')
const express = require('express')
const { OK, NOT_FOUND } = require('http-status-codes')

const { notFoundMiddleware } = require('../../middleware/not-found.middleware')

describe('middlewares/not-found.middleware', () => {
  let app

  beforeEach(() => {
    app = express()
  })

  it('Must get a 404 error', async () => {
    let response
    app.use('/home', (req, res) => res.json({ status: 'ok' }))
    app.use(notFoundMiddleware)

    response = await request(app)
      .get('/home')
      .send()

    expect(response.status).toBe(OK)

    response = await request(app)
      .get('/other')
      .send()

    expect(response.status).toBe(NOT_FOUND)
  })
})
