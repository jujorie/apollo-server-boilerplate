const Express = require('express')
const request = require('supertest')
const { NOT_FOUND } = require('http-status-codes')

const {
  applyCommonMiddleware
} = require('../../middleware/apply-common-middleware')

describe('middleware/commonMiddleware', () => {
  let app

  beforeEach(() => {
    app = Express()
    applyCommonMiddleware(app)
  })

  it('Must to fail with 404', async () => {
    const response = await request(app)
      .get('/test')
      .send()

    expect(response.status).toBe(NOT_FOUND)
  })
})
