const request = require('supertest')
const { app } = require('../../../app')

describe('resolvers/ping', () => {
  it('Must resolve the query ping', async () => {
    const response = await request(app)
      .post('/graphql')
      .send({
        query: `
        query {
          ping
        }
      `
      })
    expect(response.status).toBe(200)
  })
})
