const { createServer } = require('http')
const { app } = require('./app')
const config = require('./config')

const port = config.get('port')

const server = createServer(app).listen(port, () => {
  console.log(`Listening @ ${port}`)
})

module.exports = { server }
