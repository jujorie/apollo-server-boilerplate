---
to: lib/graphql/resolvers/<%= h.changeCase.param(h.inflection.singularize(name)) %>.resolver.js
---
<%
className = h.changeCase.pascal(h.inflection.singularize(name))
propertyName = h.changeCase.camelCase(h.inflection.pluralize(name))
%>
/**
* @see https://www.apollographql.com/docs/graphql-tools/resolvers/
*/
module.exports = {
  Query: {
    <%= propertyName %>: (parent, args, context) => Promise.resolve([
      {
        id: 1
      },
      {
        id: 2
      },
      {
        id: 3
      }
    ])
  }
}
