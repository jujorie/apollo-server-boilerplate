---
to: lib/graphql/types/<%= h.changeCase.param(h.inflection.singularize(name)) %>.graphql
---
<%
className = h.changeCase.pascal(h.inflection.singularize(name))
propertyName = h.changeCase.camelCase(h.inflection.pluralize(name))
%>

type <%= className %> {
  id: Int
}

type Query {
  <%= propertyName %>: [<%= className %>]
}
