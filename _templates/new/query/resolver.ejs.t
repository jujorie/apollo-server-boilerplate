---
to: lib/graphql/resolvers/<%= h.changeCase.param(name) %>.resolver.js
---
<%
propertyName = h.changeCase.camelCase(h.inflection.pluralize(name))
%>
/**
* @see https://www.apollographql.com/docs/graphql-tools/resolvers/
*/
module.exports = {
  Query: {
    <%= propertyName %>: (parent, args, context) => 'resolver'
  }
}
