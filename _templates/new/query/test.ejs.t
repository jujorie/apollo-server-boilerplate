---
to: lib/__tests__/graphql/resolvers/<%= h.changeCase.param(name) %>.test.js
---
<%
propertyName = h.changeCase.camelCase(h.inflection.pluralize(name))
%>

const request = require('supertest')
const { app } = require('../../../app')

describe('resolvers/<%= name %>', () => {
  it('Must resolve the query <%= name %>', async () => {
    const response = await request(app).post('/graphql').send({
      query: `
        query {
          <%= propertyName %>
        }
      `
    })
    expect(response.status).toBe(200)
  })
})
