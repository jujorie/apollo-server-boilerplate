---
to: lib/graphql/types/<%= h.changeCase.param(name) %>.graphql
---
<%
propertyName = h.changeCase.camelCase(h.inflection.pluralize(name))
%>
type Query {
  <%= propertyName %>: String
}


