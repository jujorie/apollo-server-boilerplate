# Apollo Sever Boilerplate

Base boilerplate for an Graphql based express server

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Contents

* [Requirements](#requirements)
* [NPM Scripts](#npm-scripts)

## Requirements

* [nodejs ^10.16.3](https://nodejs.org)
* [npm ^6.13.1](https://nodejs.org)

## NPM Scripts

### Start

Start a server

```sh
npm start
```

### Start Development

Starts server using [nodemon](https://nodemon.io/)

```sh
npm run start:dev
```

### New Component

Adds a new component to the boilerplate

```sh
npm run new
```

### Lint

run all linters. js, json, graphql and markdown

```sh
npm run lint
```
